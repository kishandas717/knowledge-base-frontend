# KnowledgebaseFrontend

This project was generated with [Angular CLI] version 10.1.1.

## Install NPM Packages

Run `npm install` in the project root directory. 

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Production 

Change the api attribute with the remote URL in the environment.prod.ts file inside environment folder.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
