import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../../models/user.model';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user: User;
  loginForm: FormGroup;
  loginErrorMessage = '';
  loginSuccessMessage = '';

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private loginService: LoginService) { }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  login(): void {
    this.loginErrorMessage = '';
    this.loginSuccessMessage = '';
    this.user = {
      email: this.loginForm.value.email,
      password: this.loginForm.value.password
    };

    this.loginService.login(this.user)
      .subscribe(response => {
        console.log(response);
        if (response.success === true) {
          this.loginSuccessMessage = response.message;
          localStorage.setItem('email', response.data.email);
          localStorage.setItem('username', response.data.username);
          localStorage.setItem('token', response.token);
          setTimeout(() => {
            this.router.navigate(['/profile']);
          }, 500);
        }
      }, error => {
        console.log(error.error);
        if (error.error.errors && error.error.errors.length > 0) {
          for (let i = 0; i <= error.error.errors.length; i++) {
            const errorMessage = error.error.errors[i];
            if (errorMessage) {
              this.loginErrorMessage += errorMessage.msg + '. ';
            }
          }
        }
        if (error.error.success === false) {
          this.loginErrorMessage = error.error.message;
        }
      });
  }

}
