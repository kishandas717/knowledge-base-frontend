import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../../models/user.model';
import { RegisterService } from './register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  user: User;
  registerForm: FormGroup;
  registerErrorMessage = '';
  registerSuccessMessage = '';

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private registerService: RegisterService
  ) { }

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      username: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  register(): void {
    this.registerSuccessMessage = '';
    this.registerErrorMessage = '';
    this.user = {
      username: this.registerForm.value.username,
      email: this.registerForm.value.email,
      password: this.registerForm.value.password
    };

    this.registerService.register(this.user)
      .subscribe(response => {
        if (response.success === true) {
          this.registerSuccessMessage = response.message;
          setTimeout(() => {
            this.router.navigate(['/login']);
          }, 500);
        }
      }, error => {
        console.log(error.error);
        if (error.error.errors && error.error.errors.length > 0) {
          for (let i = 0; i <= error.error.errors.length; i++) {
            const errorMessage = error.error.errors[i];
            if (errorMessage) {
              this.registerErrorMessage += errorMessage.msg + '. ';
            }
          }
        }
        if (error.error.success === false) {
          this.registerErrorMessage = error.error.message;
        }
      });
  }

}
