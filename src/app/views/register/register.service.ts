import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../../models/user.model';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class RegisterService {

    registerApi = environment.api + '/auth';
    constructor(private http: HttpClient) { }

    register(payload: User): Observable<any> {
        return this.http.post(this.registerApi + '/signup', payload, { headers: { 'Content-Type': 'application/json', skip: 'true' } });
    }
}

