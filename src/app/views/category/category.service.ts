import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { Category } from '../../models/category.model';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json'})
  };

  categoryApi = environment.api + '/category';
  constructor(private http: HttpClient) { }

  create(category: Category): Observable<any> {
    return this.http.post<any>(this.categoryApi, category, this.httpOptions);
  }

  getAllCategory(): Observable<Category[]> {
    return this.http.get<Category[]>(this.categoryApi, this.httpOptions);
  }

  getOneCategory(categoryId: number): Observable<Category> {
      return this.http.get<Category>(this.categoryApi + '/' + categoryId, this.httpOptions);
  }

  update(category: Category, categoryId: number): Observable<Category> {
    return this.http.put<Category>(this.categoryApi + '/' + categoryId, category, this.httpOptions);
  }

  delete(categoryId: number): Observable<any> {
    return this.http.delete(this.categoryApi + '/' + categoryId, this.httpOptions);
  }
}
