import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Category } from 'src/app/models/category.model';
import { CategoryService } from './category.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = ['id', 'name', 'description', 'action'];
  dataSource: MatTableDataSource<Category>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  categoryForm: FormGroup;
  categories: Category[];
  category: Category;

  constructor(
    private categoryService: CategoryService,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar) { }

  ngOnInit(): void {

    this.getCategories();

    this.categoryForm = this.formBuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required]
    });
  }

  ngAfterViewInit(): void {
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getCategories(): void {
    this.categoryService.getAllCategory()
      .subscribe(response => {
        this.categories = response;
        this.dataSource = new MatTableDataSource(this.categories);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }

  create(): void {
    this.category = {
      name: this.categoryForm.value.name,
      description: this.categoryForm.value.description
    };

    this.categoryService.create(this.category)
      .subscribe(response => {
        if (response.success === true) {
          this.snackBar.open(response.message, 'Done', {
            duration: 2000,
          });
          this.getCategories();
        }
      }, error => {
        if (error.error.success === false) {
          this.snackBar.open(error.error.message, 'OK', {
            duration: 2000,
          });
        }
      });
    this.categoryForm.reset();
  }

  delete(id): void {

    this.categoryService.delete(id)
      .subscribe(response => {
        if (response.success === true) {
          this.snackBar.open(response.message, 'Done', {
            duration: 2000,
          });
          this.getCategories();
        }
      }, error => {
        if (error.error.success === false) {
          this.snackBar.open(error.error.message, 'OK', {
            duration: 2000,
          });
        }
      });
  }

}
