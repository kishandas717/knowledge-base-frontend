import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CategoryComponent } from './category.component';
import { CategoryRoutingModule } from './category-routing.module';
import { AngularMaterialModule } from '../../material/angular-material.module';
import { CategoryService } from './category.service';

@NgModule({
  declarations: [CategoryComponent],
  imports: [
    CommonModule,
    CategoryRoutingModule,
    AngularMaterialModule,
    ReactiveFormsModule
  ],
  providers: [CategoryService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CategoryModule { }
