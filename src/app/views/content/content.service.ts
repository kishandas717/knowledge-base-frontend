import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { Content } from '../../models/content.model';

@Injectable({
    providedIn: 'root'
})
export class ContentService {

    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    contentApi = environment.api + '/content';
    constructor(private http: HttpClient) { }

    create(content: Content): Observable<any> {
        return this.http.post<any>(this.contentApi, content, this.httpOptions);
    }

    getAllContent(): Observable<Content[]> {
        return this.http.get<Content[]>(this.contentApi, this.httpOptions);
    }

    getOneContent(contentId: number): Observable<Content> {
        return this.http.get<Content>(this.contentApi + '/' + contentId, this.httpOptions);
    }

    update(content: Content, contentId: number): Observable<any> {
        return this.http.put<any>(this.contentApi + '/' + contentId, content, this.httpOptions);
    }

    delete(contentId: number): Observable<any> {
        return this.http.delete(this.contentApi + '/' + contentId, this.httpOptions);
    }

    uploadFile(formdata: FormData): Observable<any> {
        return this.http.post(this.contentApi + '/upload', formdata);
    }
}
