import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ContentComponent } from './content.component';
import { ContentRoutingModule } from './content-routing.module';
import { AngularMaterialModule } from '../../material/angular-material.module';
import { ContentService } from './content.service';
import { CategoryService } from '../category/category.service';

@NgModule({
  declarations: [ContentComponent],
  imports: [
    CommonModule,
    ContentRoutingModule,
    AngularMaterialModule,
    ReactiveFormsModule
  ],
  providers: [ContentService, CategoryService]
})
export class ContentModule { }
