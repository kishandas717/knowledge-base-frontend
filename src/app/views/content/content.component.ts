import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Content } from 'src/app/models/content.model';
import { ContentService } from './content.service';
import { CategoryService } from '../category/category.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Category } from 'src/app/models/category.model';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {
  displayedColumns: string[] = ['id', 'title', 'author', 'category', 'file', 'action'];
  dataSource: MatTableDataSource<Content>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  contentForm: FormGroup;
  contents: Content[];
  content: Content;
  categories: Category[];
  fileUrl: string;
  fileName: string;

  constructor(
    private contentService: ContentService,
    private categoryService: CategoryService,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private el: ElementRef
  ) { }

  ngOnInit(): void {

    this.getCategories();
    this.getContents();

    this.contentForm = this.formBuilder.group({
      title: ['', Validators.required],
      author: ['', Validators.required],
      category: ['', Validators.required]
    });
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getCategories(): void {
    this.categoryService.getAllCategory()
      .subscribe(response => {
        this.categories = response;
      });
  }

  getContents(): void {
    this.contentService.getAllContent()
      .subscribe(response => {
        this.contents = response;
        console.log(this.contents);
        this.dataSource = new MatTableDataSource(this.contents);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }

  uploadFile(): void {
    const inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#file');
    console.log(inputEl.files);
    const fileCount: number = inputEl.files.length;
    const formData = new FormData();
    if (fileCount > 0) {
      formData.append('file', inputEl.files.item(0));
      this.contentService.uploadFile(formData)
        .subscribe((response: any) => {
          this.fileUrl = response.file_location;
          this.fileName = response.file_name;
          this.snackBar.open(response.message, 'Done', {
            duration: 2000,
          });
        },
          error => {
            this.fileUrl = '';
            this.snackBar.open(error.error.message, 'OK', {
              duration: 2000,
            });
          });
      inputEl.value = '';
    }
    else {
      this.snackBar.open('Please select file.', 'OK', {
        duration: 2000,
      });
    }
  }

  create(): void {
    this.content = {
      title: this.contentForm.value.title,
      author: this.contentForm.value.author,
      file_name: this.fileName ? this.fileName : '',
      file_url: this.fileUrl ? this.fileUrl : '',
      categoryId: this.contentForm.value.category
    };

    this.contentService.create(this.content)
      .subscribe(response => {
        if (response.success === true) {
          this.snackBar.open(response.message, 'Done', {
            duration: 2000,
          });
          this.getContents();
        }
      }, error => {
        if (error.error.success === false) {
          this.snackBar.open(error.error.message, 'OK', {
            duration: 2000,
          });
        }
      });
  }

  delete(id): void {

    this.contentService.delete(id)
      .subscribe(response => {
        if (response.success === true) {
          this.snackBar.open(response.message, 'Done', {
            duration: 2000,
          });
          this.getContents();
        }
      }, error => {
        if (error.error.success === false) {
          this.snackBar.open(error.error.message, 'OK', {
            duration: 2000,
          });
        }
      });
  }

}
