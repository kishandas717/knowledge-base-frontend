import { Component, OnInit } from '@angular/core';
import { MenuItem } from '../../models/menu-item';
@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.css']
})
export class SidemenuComponent implements OnInit {
  menuItems: MenuItem[] = [
    {
      displayName: 'Dashboard',
      iconName: 'dashboard',
      route: 'dashboard'
    },
    {
      displayName: 'Profile',
      iconName: 'account_box',
      route: 'profile'
    },
    {
      displayName: 'Category',
      iconName: 'category',
      route: 'category'
    },
    {
      displayName: 'Content',
      iconName: 'description',
      route: 'content'
    }
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
