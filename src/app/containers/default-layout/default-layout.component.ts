import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { MatSidenavContainer } from '@angular/material/sidenav';
import { Router } from '@angular/router';

@Component({
  selector: 'app-default-layout',
  templateUrl: './default-layout.component.html',
  styleUrls: ['./default-layout.component.css']
})
export class DefaultLayoutComponent implements OnInit {
  public sidebarMinimized = false;
  mode = 'push';
  @ViewChild(MatSidenavContainer, { static: true }) sideNavContainer: MatSidenavContainer;
  @HostListener('window:resize', ['$event'])
  onResize(event): void {
    if (event.target.innerWidth <= 1024) {
      this.sideNavContainer.close();
      this.mode = 'over';
    }
    else {
      this.mode = 'push';
    }
  }


  constructor(private router: Router, ) { }

  ngOnInit(): void {
  }

  toggleSideNav(): void {
    this.sideNavContainer._drawers.first.toggle();
  }

  logout(): void {
    localStorage.clear();
    this.router.navigate(['login']);
  }
}
