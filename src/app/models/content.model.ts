export interface Content {
    id?: number;
    title?: string;
    author?: string;
    file_url?: string;
    file_name?: string;
    categoryId?: number;
}
