import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpClient
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment';
import { catchError, filter, take, switchMap, flatMap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { LoginService } from '../app/views/login/login.service';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptor implements HttpInterceptor {
  getTokenApi = environment.api + '/admin/token';
  constructor(public http: HttpClient, private router: Router, private loginService: LoginService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = localStorage.getItem('token');

    if (request.headers.get('skip')) {
      return next.handle(request);
    }

    if (token) {
      request = request.clone({
        setHeaders: {
          token
        }
      });
    }
    return next.handle(request);
  }
}
